#!/usr/bin/env python                                                                                                                                                                                              
# -*- coding: utf-8 -*-                                                                                                                                                                                            

import numpy as np
import scipy
import scipy.signal
import re
import numpy.ma as ma

# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0                                                                                                                                      






####################################################################################
#
#  Prony methods for fitting dumped exponentials. See gr-qc/0701086.
#
###################################################################################

# Direct Matrix pencil method. From Hua Y Sarkar T, "On SVD for
# estimating generalized eigenvalues of singular matrix pencil in
# noise", 1991., IEEE International Sympoisum on Circuits and Systems

# data = complex signal.
# T = dt of the signal.  
# p = number of exponential components. If p==0 autodetected.  
# precision = input precision for detecting p.  
# L = model order. If L==0 autodetected as len(x)/3

# Works well for frequency, not for tau or phase.
# Amplitude is correct only if phase is 0.


def MatrixPencil(data,T,p=0,precision=1e-3,L=0):
    N = len(data)
    if L==0:
        L=N/3 
    Y0 = np.zeros((N-L,L),dtype=complex)
    Y1 = np.zeros((N-L,L),dtype=complex)
    for i in range(N-L):
        for j in range(L):
            Y0[i,j]=data[i+j]
            Y1[i,j]=data[i+j+1]
    U0,s0,VH0 = np.linalg.svd(Y0)
    U1,s1,VH1 = np.linalg.svd(Y1)
    if p == 0:
        for ss in s0:
            if float(ss)/s0[0]>precision:
                p=p+1
        print 'first 10 s ', s0[:10]
    Y0 = np.zeros((N-L,L),dtype=complex)
    Y1 = np.zeros((N-L,L),dtype=complex)
    for i in range(N-L):
        for j in range(L):
            Y0[i,j]=data[i+j]
            Y1[i,j]=data[i+j+1]
    U0,s0,VH0 = np.linalg.svd(Y0)
    U1,s1,VH1 = np.linalg.svd(Y1)
    if p == 0:
        for ss in s0:
            if float(ss)/s0[0]>precision:
                p=p+1
        print 'first 10 s ', s0[:10]
    Sigma0 = np.diag(s0[:p])
    Sigma1 = np.diag(s1[:p])
    U0 = U0[:,:p]
    U1 = U1[:,:p]
    VH0 = VH0[:p,:]
    VH1 = VH1[:p,:]
    Y1 = np.dot(U1,np.dot(Sigma1,VH1))
    M = np.dot(np.linalg.inv(Sigma0),np.dot(U0.conj().T,np.dot(Y1,VH0.conj().T)))
    z,v = np.linalg.eig(M)
    print 'zmod = ',np.abs(z)
    print 'z = ', z
    alpha = np.log(np.abs(z))/T
    omega = np.angle(z)/T
    Z = np.ones((N,p),dtype=complex)
    for i in range(N):
        for j in range(p):
            Z[i,j]=z[j]**i
    #ZHZ = np.ones((p,p),dtype=complex)
    #for i in range(p):
    #    for j in range(p):
    #        if np.conj(z[i])*z[j]==1:
    #            ZHZ[i,j]=N
    #        else:
    #            ZHZ[i,j]=((np.conj(z[i])*z[j])**N-1)/(np.conj(z[i])*z[j]-1)
    #ZHx = np.dot(Z.conj().T,data)
    #h = np.linalg.solve(ZHZ,ZHx)
    h = np.dot(np.linalg.inv(np.dot(Z.conj().T,Z)),np.dot(Z.conj().T,data))
    A = np.abs(h)
    phi = np.angle(h)
    return A,alpha,omega,phi
    
# From gr-qc/0701086
# Currently doesn't work, some debugging needed.

def KumaresanTufts(data,T,precision = 1e-3, L=0,p=0):
    N = len(data)
    if L==0:
        L=N/3
    X = np.zeros((N-L,L),dtype=complex)
    xl = data[L:]
    for i in range(0,N-L):
        for j in range(0,L):
            X[i,j]=data[L-1-j+i]
    
    print X.shape
    U,s,VH = np.linalg.svd(X)
    if p == 0:
        for ss in s:
            if float(ss)/s[0]>precision:
                p=p+1
        print 'first 10 s ', s[:10]
    Sigma = np.zeros((N-L,L),dtype=complex)
    for i in range(p):
        Sigma[i,i] = s[i]
    Xhat = np.dot(U,np.dot(Sigma,VH))
    XhatCross = np.dot(np.linalg.inv(np.dot(Xhat.conj().T,Xhat)),Xhat.conj().T)
    a = np.ones(L+1,dtype=complex)
    a[1:] = -np.dot(XhatCross,xl)
    zhat = np.roots(a)
    zmod = np.abs(zhat)-1
    index_sort = np.argsort(zmod)
    z = zhat[index_sort][:p]
    print 'zmodulus',zmod[index_sort][:2*p]+1
    print 'z',z
    alpha = np.log(np.abs(z))/T
    omega = np.angle(z)/T
    Z = np.ones((N,p),dtype=complex)
    for i in range(N):
        for j in range(p):
            Z[i,j]=z[j]**i
    ZHZ = np.ones((p,p),dtype=complex)
    for i in range(p):
        for j in range(p):
            if np.conj(z[i])*z[j]==1:
                ZHZ[i,j]=N
            else:
                ZHZ[i,j]=((np.conj(z[i])*z[j])**N-1)/(np.conj(z[i])*z[j]-1)
    ZHx = np.dot(Z.conj().T,data)
    h = np.linalg.solve(ZHZ,ZHx)
    A = np.abs(h)
    phi = np.angle(h)
    return A,alpha,omega,phi

# Modified least-square Prony. From gr-qc/0701086.
# Works only on noiseless data.
    
def LSProny(data,T,p=3):
    N = len(data)
    X = np.zeros((N-p,p),dtype=complex)
    xl = data[p:]
    for i in range(0,N-p):
        for j in range(0,p):
            X[i,j]=data[p-1-j+i]

    print X.shape
    a = np.ones(p+1,dtype=complex)
    a[1:] = -np.dot(np.dot(np.linalg.inv(np.dot(X.conj().T,X)),X.conj().T),xl)
    z = np.roots(a)
    print 'zmod = ', np.abs(z)
    alpha = np.log(np.abs(z))/T
    omega = np.angle(z)/T
    Z = np.ones((N,p),dtype=complex)
    for i in range(N):
        for j in range(p):
            Z[i,j]=z[j]**i
    ZHZ = np.ones((p,p),dtype=complex)
    for i in range(p):
        for j in range(p):
            if np.conj(z[i])*z[j]==1:
                ZHZ[i,j]=N
            else:
                ZHZ[i,j]=((np.conj(z[i])*z[j])**N-1)/(np.conj(z[i])*z[j]-1)
    ZHx = np.dot(Z.conj().T,data)
    h = np.linalg.solve(ZHZ,ZHx)
    A = np.abs(h)
    phi = np.angle(h)
    return A,alpha,omega,phi


# Same result as MatrixPencil, but phase and Amplitude estimation is even worse.


def ESPRITProny(data,T,p=0,precision=1e-3,L=0):

    N = len(data)
    if L==0:
        L=N/6
    H = np.zeros((N-L,L+1),dtype=complex)
    for i in range(N-L):
        for j in range(L+1):
            H[i,j]=data[i+j]
    U,d,W = np.linalg.svd(H)
    if p == 0:
        for ss in d:
            if float(ss)/d[0]>precision:
                p=p+1
        #print 'first 10 s ', d[:10]
    W0 = W[:p,:L]
    W1 = W[:p,1:L+1]
    F = np.dot(np.linalg.pinv(W0.conj().T),W1.conj().T)
    z,v = np.linalg.eig(F)
    z = np.conj(z)
    #print 'zmod = ',np.abs(z)
    #print 'z = ', z
    alpha = np.log(np.abs(z))/T
    omega = np.angle(z)/T
    V = np.ones((N,p),dtype=complex)
    for i in range(N):
        for j in range(p):
            V[i,j]=z[j]**i
    h,a,b,c = np.linalg.lstsq(V,data,rcond=-1)
    #h = np.dot(np.linalg.inv(np.dot(V.conj().T,V)),np.dot(V.conj().T,data))
    A = np.abs(h)
    phi = np.angle(h)
    return A,alpha,omega,phi

# Prony method applied in a small window around each point in the signal.
# t: time variable [len(t) == len(data)]
# dt: number of points in each window


def RollingProny(data,t,p=0,L=0,precision=1e-3,dt=200):
    T=len(t)
    omega=np.empty(T,dtype=object)
    A=np.empty(T,dtype=object)
    phi=np.empty(T,dtype=object)
    alpha = np.empty(T,dtype=object)
    dT = np.diff(t)[1]
    for i in range(T):
        if (i < dt/2 or i > T-dt/2):
            omega[i]=np.array([0])
            alpha[i]=np.array([0])
            phi[i]=np.array([0])
            A[i]=np.array([0])
        else:
            x = data[i-dt/2:i+dt/2]
            N = len(x)
            A[i],alpha[i],omega[i],phi[i] = ESPRITProny(x,dT,L=L,p=0,precision=precision)
    f = omega/(2*np.pi)
    tau = np.empty(T,dtype=object)
    for i,a in enumerate(alpha):
        tau[i]=np.zeros(len(a))
        for j,aa in enumerate(a):
            if aa != 0:
                tau[i][j]=1.0/aa
    return A,tau,f,phi

#Returns masked arrays, columns ordered by frequency
def RollingProny_masked(data,t,p=0,L=0,precision=1e-3,dt=200,maxp=4):
    T=len(t)
    omega=np.zeros((T,maxp))
    A=np.zeros((T,maxp))
    phi=np.zeros((T,maxp))
    alpha = np.zeros((T,maxp))
    dT = np.diff(t)[1]
    for i in range(T):
        if (i < dt/2 or i > T-dt/2):
            pass
        else:
            x = data[i-dt/2:i+dt/2]
            N = len(x)
            try :
                A_p,alpha_p,omega_p,phi_p = ESPRITProny(x,dT,L=L,p=0,precision=precision)
                tr=min(maxp,len(A_p))
                idx=np.argsort(omega_p)
                A[i,:tr] = A_p[idx][:tr]
                alpha[i,:tr] = alpha_p[idx][:tr]
                omega[i,:tr] = omega_p[idx][:tr]
                phi[i,:tr] = phi_p[idx][:tr]
            except:
                pass;
    f = omega/(2*np.pi)
    tau = np.zeros((T,maxp))
    for i,a in enumerate(alpha):
        for j,aa in enumerate(a):
            if aa != 0:
                tau[i,j]=1.0/aa
    A = ma.masked_equal(A,0)
    tau = ma.masked_equal(tau,0)
    f = ma.masked_equal(f,0)
    phi = ma.masked_equal(phi,0)
    return A,tau,f,phi
