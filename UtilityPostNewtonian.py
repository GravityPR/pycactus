#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy 
import scipy.signal 
import scipy.integrate
import re
import lal
import lalsimulation
from UtilityUnits import *

# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0

# From 
# y = [x,phi]
# x = 
def TaylorT4function(t,y,m):
    x = y[0]
    phi = y[1]
    gamma = 0.577216
    dxdt = 16.0/(5*m)*x**5*(1.0-487.0/168*x + 4.0*np.pi*x**(1.5) + 274229.0/72576*x**2 
    - 254.0/21*np.pi*x**(2.5) + (178384023737.0/3353011200 + 1475.0/192*np.pi**2
    - 1712.0/105*gamma - 856.0/105*np.log(16*x))*x**3 + 3310.0/189*np.pi*x**(3.5))
    dphidt = x**(1.5)/m
    return [dxdt,dphidt]

def TaylorT4(m,Omega0,tm,dx):
    eq = scipy.integrate.ode(TaylorT4function).set_integrator("dopri5")
    x0 = (m*Omega0)**(2.0/3)
    phi0 = 0
    t0 = 0
    eq.set_initial_value([x0,phi0],t0).set_f_params(m)
    dt = 0.25*dx*128
    phi = np.zeros(np.ceil(tm/dt))
    time  = np.zeros(np.ceil(tm/dt))
    j = 0
    while eq.successful() and eq.t < tm:
        eq.integrate(eq.t+dt)
        phi[j]=eq.y[1]
        time[j] = eq.t
        j+=1
    return phi,time
    
############################################################################
#                                                                          #
# From now on there is a control python interface to LIGO LAL library      #
#                                                                          #
############################################################################

# TO DO: write here instructions for downloading and building LAL and
# its python interface.


# Since lalsim-inspiral program seems to be not working with Eccentric 
# binaries, here is a scratch of a function to use lalsimulation to generate
# Post Newtonian waveforms, adapted to PyCactus conventions.
# All the inputs are in CU.
def lalsim_inspiral(m1,m2,deltaT,f0,d_ext,e0):
    m1_Kg=m1*M_sun
    m2_Kg=m2*M_sun
    deltaT_s=deltaT*CU_to_ms/1000
    f0_hz=f0/CU_to_ms*1000
    d_ext_m = d_ext*CU_to_km*1000
    if e0 > 0:
        h = lalsimulation.SimInspiralEccentricTDPNGenerator(0,deltaT_s,m1_Kg,m2_Kg,f0_hz,0,d_ext_m,0,-e0,0,4)
        h=h[0].data.data + 1j*h[1].data.data
    elif e0 == 0:
        h = lalsimulation.SimInspiralTaylorT4PNGenerator(0,1,deltaT_s,m1_Kg,m2_Kg,f0_hz,0,d_ext_m,0,0,0,lalsimulation.SIM_INSPIRAL_TIDAL_ORDER_0PN,0,4)
        h=h[0].data.data + 1j*h[1].data.data
    return h

def lal_EOB(m1,m2,deltaT,f0,d_ext):
    m1_Kg=m1*M_sun
    m2_Kg=m2*M_sun
    deltaT_s=deltaT*CU_to_ms/1000
    f0_hz=f0/CU_to_ms*1000
    d_ext_m = d_ext*CU_to_km*1000
    

def distinguishability(h1,h2,f,f0,f1):
    print'entering distinguishability'
    sn=np.loadtxt('/opt/ligo/LAL/lalsuite/lalsimulation/src/LIGO-T0900288-v3-ZERO_DET_high_P.txt',unpack=True)
    print'noise curve loaded'
    sn = np.interp(f,sn[0],sn[1])
    sn = np.exp(2*np.log(sn))
    print 'interpolation done'
    idx0=np.searchsorted(f,f0)
    idx1=np.searchsorted(f,f1)
    print idx0,idx1,f[idx0],f[idx1]
    SNR1=np.sqrt(4*scipy.integrate.simps(np.abs(h1[idx0:idx1])**2/sn[idx0:idx1],x=f[idx0:idx1]))
    SNR2=np.sqrt(4*scipy.integrate.simps(np.abs(h2[idx0:idx1])**2/sn[idx0:idx1],x=f[idx0:idx1]))
    print 'SNR1= ',SNR1
    print 'SNR2= ',SNR2
    #dh=4*scipy.integrate.simps(np.abs(h1[idx0:idx1]-h2[idx0:idx1])**2/sn[idx0:idx1],x=f[idx0:idx1])
    diff = lambda x: scipy.integrate.simps(np.abs(h1[idx0:idx1]-h2[idx0:idx1]*np.exp(1j*(2*np.pi*f[idx0:idx1]*x[0]+x[1])))**2/sn[idx0:idx1],x=f[idx0:idx1])
    par=scipy.optimize.minimize(diff,[0,0],method='Nelder-Mead')
    return np.sqrt(4*diff([par.x[0],par.x[1]]))
    #return np.sqrt(dh)

def AdLigoNoise(f):
    sn=np.loadtxt('/opt/ligo/LAL/lalsuite/lalsimulation/src/LIGO-T0900288-v3-ZERO_DET_high_P.txt',unpack=True)
    sn = np.interp(f,sn[0],sn[1])
    sn = np.exp(2*np.log(sn))
    return sn
