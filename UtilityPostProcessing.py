import numpy as np
import scipy as scipy
import h5py
from UtilityUnits import *

### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingScalar(Sc) :
    OUT = dict()
    OUT['time'] = np.array(Sc['ScMAX']['time'])
    OUT['t_ms'] = CU_to_ms * OUT['time']
    OUT['eta']  = ( (Sc['ScMAX']['hydro_Qxx']-Sc['ScMAX']['hydro_Qyy']) + 2*1j*Sc['ScMAX']['hydro_Qxy']
                  ) / (Sc['ScMAX']['hydro_Qxx']+Sc['ScMAX']['hydro_Qyy'])
    time = OUT['time']
    ddQxy=np.diff(Sc['ScMAX']['hydro_dQxy']) / np.diff(Sc['ScMAX']['time'])
    ddQxx=np.diff(Sc['ScMAX']['hydro_dQxx']) / np.diff(Sc['ScMAX']['time'])
    ddQyy=np.diff(Sc['ScMAX']['hydro_dQyy']) / np.diff(Sc['ScMAX']['time'])
    ddQxz=np.diff(Sc['ScMAX']['hydro_dQxz']) / np.diff(Sc['ScMAX']['time'])
    ddQyz=np.diff(Sc['ScMAX']['hydro_dQyz']) / np.diff(Sc['ScMAX']['time'])
    ddQzz=np.diff(Sc['ScMAX']['hydro_dQzz']) / np.diff(Sc['ScMAX']['time'])
    h_plus  = ddQxx - ddQyy
    h_times = 2*ddQxy
    h = np.sqrt(h_plus**2+h_times**2)
    h_phi = np.angle(h_plus+1j*h_times)
    for k in range(len(h_phi)-1):
        jump = h_phi[k+1]-h_phi[k]
        if (np.abs(jump)>4):
            h_phi[(k+1):] = h_phi[(k+1):] - jump/abs(jump)*2*np.pi
    #-------
    OUT['h_plus' ] = np.zeros(time.shape)
    OUT['h_times'] = np.zeros(time.shape)
    OUT['h']       = np.zeros(time.shape)
    OUT['h_phi']   = np.zeros(time.shape)
    OUT['h_freq']  = np.zeros(time.shape)
    #-------
    OUT['h_plus' ][:-1] = h_plus[:]
    OUT['h_times'][:-1] = h_times[:]
    OUT['h_phi'][:-1]   = h_phi[:]
    OUT['h_freq'][:-2]  = np.diff(h_phi)/np.diff(time[1:])
    OUT['h'][:-1]       = h[:]
    OUT['t_max_h']      = time[np.argmax( (h*( (time[:-1]>100)*1.0)) ) ]
    return OUT

### -------------------------------------------------------
###   Compute LOCATION of MINIMUM
### -------------------------------------------------------

def PostProcessingFindMinimum(X,Y,var,position=np.array([0.0,0.0]),target=10.0,interp=True) :
    pass;
    target=target/(np.max(X)-np.min(X))
    size=np.round(target*np.array(var.shape))
    idxMAXold=np.unravel_index(np.argmin((X-position[0])**2+(Y-position[1])**2),var.shape) 

    idxMINx = np.max([0,np.int(idxMAXold[0]-size[0])]) 
    idxMAXx = np.min([var.shape[0],np.int(idxMAXold[0]+size[0])])

    idxMINy = np.max([0,np.int(idxMAXold[1]-size[1])]) 
    idxMAXy = np.min([var.shape[1],np.int(idxMAXold[1]+size[1])])
    
    ## ---- print idxMINx,idxMAXx,idxMINy,idxMAXy
    x = X[idxMINx:idxMAXx+1,idxMINy:idxMAXy+1] 
    y = Y[idxMINx:idxMAXx+1,idxMINy:idxMAXy+1] 
    v = var[idxMINx:idxMAXx+1,idxMINy:idxMAXy+1] 
    idxX,idxY = np.unravel_index(np.argmin(v),v.shape)
    position  = np.array([x[idxX,idxY],y[idxX,idxY]])
    try :
        ## --------------------------------------------------------------------------
        ## If interp it is true try to improve precision using cubic inetrpolations
        ## --------------------------------------------------------------------------
        if interp == True :
            f_int   = scipy.interpolate.interp2d(x[(idxX-2):(idxX+3),(idxY-2):(idxY+3)],
                                                 y[(idxX-2):(idxX+3),(idxY-2):(idxY+3)],
                                                 v[(idxX-2):(idxX+3),(idxY-2):(idxY+3)],
                                                 kind='cubic')
            f_min   = lambda (xx) : ( f_int(xx[0],xx[1]))
            position=scipy.optimize.fmin(f_min,position,disp=False)
    except:
        pass
    return position

### -------------------------------------------------------
###
### -------------------------------------------------------
def DirDer( var , dir=0,dx=1.0) :
    dimension=len(var.shape);
    out = np.zeros(var.shape);
    assert( dir < dimension);
    if dimension == 0 :
        pass;
        out[1:-1] = 0.5 * (var[2:]-var[:-2]);
        out[0   ] =       (var[1 ]-var[0  ]);
        out[-1  ] =       (var[-1]-var[-2 ]);
    elif dimension == 2 :
        if dir == 1 :
            out[:,1:-1] = 0.5 * (var[:,2:]-var[:,:-2]);
            out[:,0   ] =       (var[:,1 ]-var[:,0  ]);
            out[:,-1  ] =       (var[:,-1]-var[:,-2 ]);
        elif dir == 2 :
            out[1:-1,:] = 0.5 * (var[2:,:]-var[:-2,:]);
            out[0   ,:] =       (var[1 ,:]-var[0  ,:]);
            out[-1  ,:] =       (var[-1,:]-var[-2 ,:]);
        pass;
    elif dimension == 3 :
        if dir == 0 :
            out[:,:,1:-1] = 0.5 * (var[:,:,2:]-var[:,:,:-2]);
            out[:,:,0   ] =       (var[:,:,1 ]-var[:,:,0  ]);
            out[:,:,-1  ] =       (var[:,:,-1]-var[:,:,-2 ]);
        elif dir == 1 :
            out[:,1:-1,:] = 0.5 * (var[:,2:,:]-var[:,:-2,:]);
            out[:,0   ,:] =       (var[:,1 ,:]-var[:,0  ,:]);
            out[:,-1  ,:] =       (var[:,-1,:]-var[:,-2 ,:]);
        elif dir == 2 :
            out[1:-1,:,:] = 0.5 * (var[2:,:,:]-var[:-2,:,:]);
            out[0   ,:,:] =       (var[1 ,:,:]-var[0  ,:,:]);
            out[-1  ,:,:] =       (var[-1,:,:]-var[-2 ,:,:]);
        pass;
    return out/dx


 

### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_epsrho(d) :
    if not ('epsrho' in list(d)) :
        d['epsrho'] = d['eps']*d['rho']
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_sqrtg(d) :
    if not ('sqrtg' in list(d)) :
        d['sqrtg'] = np.sqrt(
                    d['gxx'] * d['gyy'] * d['gzz']+ 2.0*d['gxy']*d['gxz']*d['gyz']
                  - d['gxy'] * d['gxy'] * d['gzz']- d['gxz'] * d['gxz'] * d['gyy']
                  - d['gyz'] * d['gyz'] * d['gxx']) 
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_gUP(d) :
    if not ('gUxx' in list(d)) :
        PostProcessingCompute_sqrtg(d);
        INVdetG = (1.0/d['sqrtg']**2);
        d['gUxx'] =  INVdetG * (d['gyy']*d['gzz']-d['gyz']*d['gyz'] );
        d['gUyy'] =  INVdetG * (d['gxx']*d['gzz']-d['gxz']*d['gxz'] );
        d['gUzz'] =  INVdetG * (d['gxx']*d['gxy']-d['gxy']*d['gxy'] );
        d['gUxy'] =  INVdetG * (d['gxz']*d['gyz']-d['gxy']*d['gzz'] );
        d['gUxz'] =  INVdetG * (d['gxy']*d['gyz']-d['gxz']*d['gyy'] );
        d['gUyz'] =  INVdetG * (d['gxz']*d['gxy']-d['gyz']*d['gxx'] );
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_GAMMA1(d,dx=1.0) :
    pass;
    if not ('GAMMA1xxx' in list(d)) :
        # ---------------------------------------------------------------------------
        #        1 2 3                 1  2 3             1  3 2           2 3   1
        # ---------------------------------------------------------------------------
        d['GAMMA1xxx'] =(0.5/dx) * ( DirDer(d['gxx'],0) + DirDer(d['gxx'],0) - DirDer(d['gxx'],0) )
        d['GAMMA1xxy'] =(0.5/dx) * ( DirDer(d['gxx'],1) + DirDer(d['gxy'],0) - DirDer(d['gxy'],0) )
        d['GAMMA1xxz'] =(0.5/dx) * ( DirDer(d['gxx'],2) + DirDer(d['gxz'],0) - DirDer(d['gxz'],0) )
        d['GAMMA1xyy'] =(0.5/dx) * ( DirDer(d['gxy'],1) + DirDer(d['gxy'],1) - DirDer(d['gyy'],0) )
        d['GAMMA1xyz'] =(0.5/dx) * ( DirDer(d['gxy'],2) + DirDer(d['gxz'],1) - DirDer(d['gyz'],0) )
        d['GAMMA1xzz'] =(0.5/dx) * ( DirDer(d['gxz'],2) + DirDer(d['gxz'],2) - DirDer(d['gzz'],0) )
        # ---------------------------------------------------------------------------
        d['GAMMA1yxx'] =(0.5/dx) * ( DirDer(d['gxy'],0) + DirDer(d['gxy'],0) - DirDer(d['gxx'],1) )
        d['GAMMA1yxy'] =(0.5/dx) * ( DirDer(d['gxy'],1) + DirDer(d['gyy'],0) - DirDer(d['gxy'],1) )
        d['GAMMA1yxz'] =(0.5/dx) * ( DirDer(d['gxy'],2) + DirDer(d['gyz'],0) - DirDer(d['gxz'],1) )
        d['GAMMA1yyy'] =(0.5/dx) * ( DirDer(d['gyy'],1) + DirDer(d['gyy'],1) - DirDer(d['gyy'],1) )
        d['GAMMA1yyz'] =(0.5/dx) * ( DirDer(d['gyy'],2) + DirDer(d['gyz'],1) - DirDer(d['gyz'],1) )
        d['GAMMA1yzz'] =(0.5/dx) * ( DirDer(d['gyz'],2) + DirDer(d['gyz'],2) - DirDer(d['gzz'],1) )
        # ---------------------------------------------------------------------------
        d['GAMMA1zxx'] =(0.5/dx) * ( DirDer(d['gxz'],0) + DirDer(d['gxz'],0) - DirDer(d['gxx'],2) )
        d['GAMMA1zxy'] =(0.5/dx) * ( DirDer(d['gxz'],1) + DirDer(d['gyz'],0) - DirDer(d['gxy'],2) )
        d['GAMMA1zxz'] =(0.5/dx) * ( DirDer(d['gxz'],2) + DirDer(d['gzz'],0) - DirDer(d['gxz'],2) )
        d['GAMMA1zyy'] =(0.5/dx) * ( DirDer(d['gyz'],1) + DirDer(d['gyz'],1) - DirDer(d['gyy'],2) )
        d['GAMMA1zyz'] =(0.5/dx) * ( DirDer(d['gyz'],2) + DirDer(d['gzz'],1) - DirDer(d['gyz'],2) )
        d['GAMMA1zzz'] =(0.5/dx) * ( DirDer(d['gzz'],2) + DirDer(d['gzz'],2) - DirDer(d['gzz'],2) )

### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_W(d) :
    if not ('W' in list(d)) :
        d_V2 = (    d['gxx'] * d['vel[0]'] * d['vel[0]'] 
                  + d['gyy'] * d['vel[1]'] * d['vel[1]'] 
                  + d['gzz'] * d['vel[2]'] * d['vel[2]']
                  + 2.0 * d['gxy'] * d['vel[0]'] * d['vel[1]'] 
                  + 2.0 * d['gxz'] * d['vel[0]'] * d['vel[2]'] 
                  + 2.0 * d['gyz'] * d['vel[1]'] * d['vel[2]'] 
                  )
        d['W'] = 1 / np.sqrt(1 - d_V2)
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_rhoS(d) :
    if not ('rho*' in list(d)) :
        PostProcessingCompute_sqrtg(d)
        PostProcessingCompute_W(d)
        d['rho*'] = d['sqrtg'] * d['W'] * d['rho']
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_Us(d) :
    if not ('u0' in list(d)) :
        PostProcessingCompute_W(d) 
        d['u0'] = d['W'] / d['alp']
        d['ux'] = d['W']  * (d['vel[0]'] - d['betax'] / d['alp']);
        d['uy'] = d['W']  * (d['vel[1]'] - d['betay'] / d['alp']);
        d['uz'] = d['W']  * (d['vel[2]'] - d['betaz'] / d['alp']);
        ###### ----------******************************************
        d['uD0'] =(-d['alp'] + d['gxx'] * d['vel[0]'] * d['betax'] 
                   + d['gxy'] * d['vel[0]'] * d['betay'] 
                   + d['gxz'] * d['vel[0]'] * d['betaz'] 
                   + d['gxy'] * d['vel[1]'] * d['betax'] 
                   + d['gyy'] * d['vel[1]'] * d['betay'] 
                   + d['gyz'] * d['vel[1]'] * d['betaz'] 
                   + d['gxz'] * d['vel[2]'] * d['betax'] 
                   + d['gyz'] * d['vel[2]'] * d['betay'] 
                   + d['gzz'] * d['vel[2]'] * d['betaz'] 
                ) * d['W']
        ###### ----------******************************************
        d['uDx'] = d['W'] * ( + d['gxx'] * d['vel[0]'] 
                         + d['gxy'] * d['vel[1]'] 
                         + d['gxz'] * d['vel[2]'])
        d['uDy'] = d['W'] * ( + d['gxy'] * d['vel[0]'] 
                         + d['gyy'] * d['vel[1]'] 
                         + d['gyz'] * d['vel[2]'])
        d['uDz'] = d['W'] * ( + d['gxz'] * d['vel[0]'] 
                         + d['gyz'] * d['vel[1]']
                         + d['gzz'] * d['vel[2]'])
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_betaD(d) :
    if not ('betaDx' in list(d)) :
        d['betaDx'] =  ( + d['gxx'] * d['betax'] 
                         + d['gxy'] * d['betay'] 
                         + d['gxz'] * d['betaz'])
        d['betaDy'] =  ( + d['gxy'] * d['betax'] 
                         + d['gyy'] * d['betay'] 
                         + d['gyz'] * d['betaz'])
        d['betaDz'] = (  + d['gxz'] * d['betax'] 
                         + d['gyz'] * d['betay']
                         + d['gzz'] * d['betaz'])
### -------------------------------------------------------
###
### -------------------------------------------------------
def PostProcessingCompute_Rcyl(d,kind='XZ') :
    if not ('RCyl' in list(d)) :
        if len(d['X[0]'].shape) == 3 :
            d['Xcoord'] = d['X[0]']
            d['Ycoord'] = d['X[1]']
            d['Zcoord'] = d['X[2]']
        elif len(d['X[0]'].shape) == 2 :
            if kind == 'XY' :
                d['Xcoord'] = d['X[0]']
                d['Ycoord'] = d['X[1]']
                d['Zcoord'] = np.zeros(d['X[0]'].shape)
            elif kind == 'XZ' :
                d['Xcoord'] = d['X[0]']
                d['Ycoord'] = np.zeros(d['X[0]'].shape)
                d['Zcoord'] = d['X[1]']
            elif kind == 'YZ' :
                d['Xcoord'] = np.zeros(d['X[0]'].shape)
                d['Ycoord'] = d['X[0]']
                d['Zcoord'] = d['X[1]']
        elif len(d['X[0]'].shape) == 1 :
            if kind == 'X' :
                d['Xcoord'] = d['X[0]']
                d['Ycoord'] = np.zeros(d['X[0]'].shape)
                d['Zcoord'] = np.zeros(d['X[0]'].shape)
            elif kind == 'Y' :
                d['Xcoord'] = np.zeros(d['X[0]'].shape)
                d['Ycoord'] = d['X[0]']
                d['Zcoord'] = np.zeros(d['X[0]'].shape)
            elif kind == 'Z' :
                d['Xcoord'] = np.zeros(d['X[0]'].shape)
                d['Ycoord'] = np.zeros(d['X[0]'].shape)
                d['Zcoord'] = d['X[0]']
        d['Rcyl'] = np.sqrt(d['Xcoord']**2+d['Ycoord']**2)
        #
        #  sin_phi = np.where(d['Rcyl'] > 1e-6 , d['Ycoord']/d['Rcyl'] ,0)
        #  cos_phi = np.where(d['Rcyl'] > 1e-6 , d['Xcoord']/d['Rcyl'] ,0)
        ## Gives the desired result but RuntimeWarning: divide by zero encountered in reciprocal
        ####numpy.where(condition[, x, y])

## -------------------------------------------------------
###
### -------------------------------------------------------

def PostProcessing3dFull(d,mask_val=0.0,kind='XZ',origin=[0.0,0.0,0.0], mask_radius = 0.0 ) :
    if len(d['X[0]'].shape) == 3 :
        Xcoord = origin[0] + d['X[0]']  
        Ycoord = origin[1] + d['X[1]']  
        Zcoord = origin[2] + d['X[2]']  
    elif len(d['X[0]'].shape) == 2 :
        if kind == 'XY' :
            Xcoord = origin[0] + d['X[0]']
            Ycoord = origin[1] + d['X[1]']
            Zcoord = origin[2] + np.zeros(d['X[0]'].shape)
        elif kind == 'XZ' :
            Xcoord = origin[0] + d['X[0]']
            Ycoord = origin[1] + np.zeros(d['X[0]'].shape)
            Zcoord = origin[2] + d['X[1]']
        elif kind == 'YZ' :
            Xcoord = origin[0] + np.zeros(d['X[0]'].shape)
            Ycoord = origin[1] + d['X[0]']
            Zcoord = origin[2] + d['X[1]']
    elif len(d['X[0]'].shape) == 1 :
        if kind == 'X' :
            Xcoord = origin[0] + d['X[0]']
            Ycoord = origin[1] + np.zeros(d['X[0]'].shape)
            Zcoord = origin[2] + np.zeros(d['X[0]'].shape)
        elif kind == 'Y' :
            Xcoord = origin[0] + np.zeros(d['X[0]'].shape)
            Ycoord = origin[1] + d['X[0]']
            Zcoord = origin[2] + np.zeros(d['X[0]'].shape)
        elif kind == 'Z' :
            Xcoord = origin[0] + np.zeros(d['X[0]'].shape)
            Ycoord = origin[1] + np.zeros(d['X[0]'].shape)
            Zcoord = origin[2] + d['X[0]']
    #######################
    d['maskR'] = 1.0 * ( (Xcoord**2+Ycoord**2+Zcoord**2) >= mask_radius**2 )

    # ---- Now doing some post processing
    d['sqrtg'] = np.sqrt(d['gxx']*d['gyy']*d['gzz']+2.0*d['gxy']*d['gxz']*d['gyz']
                  - d['gxy'] * d['gxy'] * d['gzz']- d['gxz'] * d['gxz'] * d['gyy']
                  - d['gyz'] * d['gyz'] * d['gxx']) 
    d['V2'] = ( d['gxx'] * d['vel[0]'] * d['vel[0]'] 
      + d['gyy'] * d['vel[1]'] * d['vel[1]'] 
      + d['gzz'] * d['vel[2]'] * d['vel[2]']
      + 2.0 * d['gxy'] * d['vel[0]'] * d['vel[1]'] 
      + 2.0 * d['gxz'] * d['vel[0]'] * d['vel[2]'] 
      + 2.0 * d['gyz'] * d['vel[1]'] * d['vel[2]'] 
       )
    d['W'] = 1 / np.sqrt(1 - d['V2'])
    d['u0'] = d['W'] / d['alp']
    d['ux'] = d['W']  * (d['vel[0]'] - d['betax'] / d['alp']);
    d['uy'] = d['W']  * (d['vel[1]'] - d['betay'] / d['alp']);
    d['uz'] = d['W']  * (d['vel[2]'] - d['betaz'] / d['alp']);
    ###### ----------******************************************
    d['uD0'] =(-d['alp'] + d['gxx'] * d['vel[0]'] * d['betax'] 
               + d['gxy'] * d['vel[0]'] * d['betay'] 
               + d['gxz'] * d['vel[0]'] * d['betaz'] 
               + d['gxy'] * d['vel[1]'] * d['betax'] 
               + d['gyy'] * d['vel[1]'] * d['betay'] 
               + d['gyz'] * d['vel[1]'] * d['betaz'] 
               + d['gxz'] * d['vel[2]'] * d['betax'] 
               + d['gyz'] * d['vel[2]'] * d['betay'] 
               + d['gzz'] * d['vel[2]'] * d['betaz'] 
            ) * d['W']
    ###### ----------******************************************
    d['uDx'] = d['W'] * ( + d['gxx'] * d['vel[0]'] 
                     + d['gxy'] * d['vel[1]'] 
                     + d['gxz'] * d['vel[2]'])
    d['uDy'] = d['W'] * ( + d['gxy'] * d['vel[0]'] 
                     + d['gyy'] * d['vel[1]'] 
                     + d['gyz'] * d['vel[2]'])
    d['uDz'] = d['W'] * ( + d['gxz'] * d['vel[0]'] 
                     + d['gyz'] * d['vel[1]']
                     + d['gzz'] * d['vel[2]'])
    ###### ----------******************************************
    d['betaDx'] =  ( + d['gxx'] * d['betax'] 
                     + d['gxy'] * d['betay'] 
                     + d['gxz'] * d['betaz'])
    d['betaDy'] =  ( + d['gxy'] * d['betax'] 
                     + d['gyy'] * d['betay'] 
                     + d['gyz'] * d['betaz'])
    d['betaDz'] = (  + d['gxz'] * d['betax'] 
                     + d['gyz'] * d['betay']
                     + d['gzz'] * d['betaz'])
    ###### ----------******************************************
    d['rho*']   = d['sqrtg'] * d['W'] * d['rho']
    d['e0']     = d['rho'] *( 1.0 + d['eps'] ) + d['press']
    #d['TmuDmu'] = 3  * d['press'] - d['rho'] * ( 1 + d['eps'] )
    d['TmuDmu'] = 4  * d['press'] - d['e0']
    d['T0D0']   = d['e0'] * d['u0'] * d['uD0']  + d['press']
    ##--------------------------------------------------------
    d['T0Dx']   = d['e0'] * d['u0'] * d['uDx']  + d['press']
    d['T0Dy']   = d['e0'] * d['u0'] * d['uDy']  + d['press']
    d['T0Dphi'] = Xcoord * d['T0Dy'] - Ycoord * d['T0Dx']
    ###### ----------******************************************
    d['R']     = np.sqrt(Xcoord**2+Ycoord**2+Zcoord**2)
    d['Rcyl']  = np.sqrt(Xcoord**2+Ycoord**2)
    ###### ----------******************************************
    d['Omega'] = (Xcoord * d['uy'] - Ycoord * d['ux'] ) / d['u0'] / (d['Rcyl']**2+1e-20)
    d['gtphi']    = - Ycoord * d['betaDx'] + Xcoord * d['betaDy'] 
    d['gphiphi']  = ( d['gxx'] * Ycoord * Ycoord
                    + d['gyy'] * Xcoord * Xcoord
                    -2* d['gxy'] * Xcoord * Ycoord
                    ) 
    d['gtt'] =( - d['alp'] * d['alp']  
               + d['betaDx'] * d['betax'] 
               + d['betaDy'] * d['betay'] 
               + d['betaDz'] * d['betaz'] )
    d['omega']    = - d['gtphi'] / (d['gphiphi']+1e-20)
    ######################################################################
    ##  d['Omega'] and d['omega'] can be calculated where d['Rcyl'] is
    ##  almost zero. In that case the compuation is not correct.
    ##  For this point recompute the values using a second order correction
    ##  (only fitting in the inner direction)
    ##
    ##   There are two possible case depending on teh fact that the data
    ##   3d or 3d or 1d 
    ##
    ######################################################################
    if len(d['Rcyl'].shape) == 3 :
        k,i,j=np.where(d['Rcyl']< 1e-12)
        for idx in range(i.size) : 
            ## d['omega'][i[idx],j[idx]] = d['omega'][i[idx],j[idx]+1]
            d['Omega'][k[idx],i[idx],j[idx]] = (1/6.0) * (
                          4*d['Omega'][k[idx],i[idx],j[idx]+1]-d['Omega'][k[idx],i[idx],j[idx]+2]
                         +4*d['Omega'][k[idx],i[idx],j[idx]-1]-d['Omega'][k[idx],i[idx],j[idx]-2]
                         )
            d['omega'][k[idx],i[idx],j[idx]] = (1/6.0) * (
                          4*d['omega'][k[idx],i[idx],j[idx]+1]-d['omega'][k[idx],i[idx],j[idx]+2]
                         +4*d['omega'][k[idx],i[idx],j[idx]-1]-d['omega'][k[idx],i[idx],j[idx]-2]
                       )
    elif len(d['Rcyl'].shape) == 2 :
        i,j=np.where(d['Rcyl']< 1e-12)
        for idx in range(i.size) : 
            ## d['omega'][i[idx],j[idx]] = d['omega'][i[idx],j[idx]+1]
            d['Omega'][i[idx],j[idx]] = (1/6.0) * (
                          4*d['Omega'][i[idx],j[idx]+1]-d['Omega'][i[idx],j[idx]+2]
                         +4*d['Omega'][i[idx],j[idx]-1]-d['Omega'][i[idx],j[idx]-2]
                         )
            d['omega'][i[idx],j[idx]] = (1/6.0) * (
                          4*d['omega'][i[idx],j[idx]+1]-d['omega'][i[idx],j[idx]+2]
                         +4*d['omega'][i[idx],j[idx]-1]-d['omega'][i[idx],j[idx]-2]
                       )
    elif len(d['Rcyl'].shape) == 1 :
        j=np.where(d['Rcyl']< 1e-12)
        for idx in range(i.size) : 
            ## d['omega'][i[idx],j[idx]] = d['omega'][i[idx],j[idx]+1]
            d['Omega'][j[idx]] = (1/6.0) * (
                          4*d['Omega'][j[idx]+1]-d['Omega'][j[idx]+2]
                         +4*d['Omega'][j[idx]-1]-d['Omega'][j[idx]-2]
                         )
            d['omega'][j[idx]] = (1/6.0) * (
                          4*d['omega'][j[idx]+1]-d['omega'][j[idx]+2]
                         +4*d['omega'][j[idx]-1]-d['omega'][j[idx]-2]
                       )

    ###### ----------******************************************
    #d['cos(phi)'] = Xcoord / (d['Rcyl']+1e-20)
    #d['sin(phi)'] = Ycoord / (d['Rcyl']+1e-20)
    ##   (-2.*tmp.T0D0+tmp.TmuDmu) .*d.sqrtg .*d.alp)
    ##--------------------------------------------------------
    d['M'] = ( ( -2 * ( ( d['rho'] * (1 + d['eps']) + d['press'] ) 
                        * d['u0'] * d['uD0']  
                      + d['press']
                      ) 
               + (3.0 * d['press'] - d['rho'] * ( 1.0 + d['eps'] ) ) 
               ) * d['sqrtg'] * d['alp'])
    d['J'] = d['T0Dphi'] * d['sqrtg'] * d['alp']
    d['T'] = 0.5 * d['Omega'] *  d['T0Dphi'] * d['sqrtg'] * d['alp']
    # -------------------------------------------------------
    #  d['g_rot'] is the term entering in the rotation Law
    #          = r^2 sin^2(theta) exp(-2 nu)
    # -------------------------------------------------------
    if mask_val < 0 :
        d['mask']     = 1.0*(d['rho']> d['rho'].max()*np.abs(mask_val))
    else :
        d['mask']     = 1.0*(d['rho']> mask_val)

    d['g_rot']    = - d['gphiphi']/d['gtt']
    d['NUM_rot']  = (d['Omega']-d['omega'])*d['g_rot'] 
    d['DEN_rot']  = 1-(d['Omega']-d['omega'])**2 *d['g_rot'] 
    d['LAW_rot']  = d['NUM_rot']/d['DEN_rot']
    ###### ----------******************************************
    # Compute derivate values for B
    ###### ----------******************************************

    if 'Bvec[0]' in list(d) :
        d['Br'] =( (d['Bvec[0]']*Xcoord+d['Bvec[1]']*Ycoord+d['Bvec[2]']*Zcoord) 
                    / (d['R']+1e-20))
        d['BDx'] = d['gxx'] * d['Bvec[0]'] + d['gxy'] * d['Bvec[1]'] + d['gxz']*d['Bvec[2]'];
        d['BDy'] = d['gxy'] * d['Bvec[0]'] + d['gyy'] * d['Bvec[1]'] + d['gyz'] * d['Bvec[2]'];
        d['BDz'] = d['gxz'] * d['Bvec[0]'] + d['gyz'] * d['Bvec[1]'] + d['gzz'] * d['Bvec[2]'];
        d['B2']  = d['Bvec[0]']  *  d['BDx'] + d['Bvec[1]']  *  d['BDy'] + d['Bvec[2]']  *  d['BDz'];
        d['bst']   = d['W']  *  (d['BDx']  *  d['vel[0]'] + d['BDy']  *  d['vel[1]'] + d['BDz']  *  d['vel[2]']);
        d['b2']    = (d['B2'] + d['bst']  *  d['bst']) / (d['W']  *  d['W']);
        d['EMt00'] = (d['B2'] - 0.5 * d['b2']) ;
        #%%%%%%%---------------------------------------------------
        #%%% 
        #%%%%%%%--------------------------------------------------- 
        #d['Bphi   = (d['X  *  d['Bvec[1]  - d['Y  *  d['Bvec[0]  ) d['/ (d['Rcyl + 1e10*(d['Rcyl==0))d['^2;
        #d['BDphi  = (d['X  *  d['BDy - d['Y  *  d['BDx ) ;
        #d['Brho   = (d['X  *  d['Bvec[0] + d['Y  *  d['Bvec[1] + d['Z  *  d['Bvec[2]) d['/ (d['R + 1e10*(d['R==0))d['^2;
        #d['Btheta = ( ((d['X  *  d['Bvec[1] + d['Y  *  d['Bvec[1] )  *  d['Z )  d['/ (d['Rcyl + 1e10*(d['Rcyl==0)) d['d['d['
        #             - d['Bvec[2]  *  d['Rcyl) d['/ (d['R + 1e10*(d['R==0))d['^2;
        #d['BtorNORM2 = d['Bphi  *  d['BDphi; 
        #d['BpolNORM2 = d['B2 - d['BtorNORM2;
        #%%%%%%%---------------------------------------------------
        #%%% A PROPOSAL FRO TOROIDAL - POLOIDAL DECOMPOSITION
        #%%% IN CASE IS NOT AXI-SIMMETRIC
        #%%%%%%%--------------------------------------------------- 
        d['Btor']  = ( ( d['BDx']  *  d['vel[0]'] + d['BDy']  *  d['vel[1]'] + d['BDz']  *  d['vel[2]'])
                       / np.sqrt(d['V2'] + 1e20 * (d['V2']==0)) );
        d['Bpolx'] = d['Bvec[0]'] - d['Btor']  *  d['vel[0]'] / np.sqrt(d['V2'] + 1e20 * (d['V2']==0));
        d['Bpoly'] = d['Bvec[1]'] - d['Btor']  *  d['vel[1]'] / np.sqrt(d['V2'] + 1e20 * (d['V2']==0));
        d['Bpolz'] = d['Bvec[2]'] - d['Btor']  *  d['vel[2]'] / np.sqrt(d['V2'] + 1e20 * (d['V2']==0));
        d['Bpol2']  =( 
            d['gxx']  *  d['Bpolx']  *  d['Bpolx']
          + d['gyy']  *  d['Bpoly']  *  d['Bpoly']
          + d['gzz']  *  d['Bpolz']  *  d['Bpolz']
          + 2  *  d['gxy']  *  d['Bpolx']  *  d['Bpoly'] 
          + 2  *  d['gxz']  *  d['Bpolx']  *  d['Bpolz']
          + 2  *  d['gyz']  *  d['Bpoly']  *  d['Bpolz']
          );
        d['EMt00pol'] = 0.5  *  (1+d['V2'])  *  d['Bpol2'];
        d['EMt00tor'] = 0.5  *  d['Btor']  *  d['Btor']  ;

