#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import getopt
 
import os
import re
import time
import numpy as np
import bz2
import gzip
#import matplotlib.pyplot as plt

### def pippo(x,search): return [item for item in list(x) if re.search(search, item)]


#################################################################################
###
###
###
###
###
###
#################################################################################

def ReadASCIIFileRE(search_dir,matchs='.asc',verbose=False,simfactory=True) :
     if type(matchs) == str :
         matchs=[matchs]
     matchedFILEs = []
     
     ### ######################################################
     ### If search_dir is not a directory checj if it is
     ### FILENAME (in this case read the content of that file) 
     ##########################################################
     if not os.path.isdir(search_dir) :
         print search_dir,"is not a directory"
         if os.path.exists(search_dir) :
             return ReadASCIIFile(search_dir,verbose=verbose,simfactory=False)
         else :
             return dict()
     ### ###################################################
     ### If simfactory is set to yes check if dir is instead
     ### the base dir not of the files but of a sifactory 
     ### simulation 
     ### ###################################################
     search_dirs = []
     #######################################################
     ### Check if the directory has a symfactory layout 
     #######################################################
     simfactory = False
     #### ---------------------------------------------
     ####  Search for tree of the kind:
     ####
     ####  search_dir/output-????/[parname]/data/Scalar  
     ####                                       /0d
     ####                                       /1d
     #### ---------------------------------------------
     for dir_entry in sorted(os.listdir(search_dir)) :
          FoundDataDir = False
          if re.match(r"^output-(\d\d\d\d)$",dir_entry) :
              ##------X---
              print "Searching on ... ",dir_entry
              if (os.path.exists(os.path.join(search_dir, "SIMFACTORY", "par"))):
                  print "Exist SIMFACTORY DIRECTORY in ... ",dir_entry
                  # Find parfile name and assume out_dir was set to $parfile
                  # print "PAR FILE FOUNDED",os.path.join(search_dir, "SIMFACTORY", "par")
                  parfiles = os.listdir(os.path.join(search_dir, "SIMFACTORY", "par"))
                  if (".svn" in parfiles): parfiles.remove(".svn")
                  ## print parfiles
                  if (len(parfiles) >= 1) :
                      #parfile = parfiles[0]
                      #parfile = re.sub(r".par$", "", parfile)
                      ##======*****************************============
                      ## We do have a proble if more that a file exist
                      ##======*****************************============
                      parfile =  os.path.splitext(parfiles[0])[0]
                      ## print parfiles[0],parfile
                      if os.path.exists(os.path.join(search_dir, dir_entry, parfile)) :
                          search_dirs.append(os.path.join(search_dir, dir_entry,parfile))
                          for subdir in ['Scalar', '0d', '1d']:
                              if (os.path.exists(os.path.join(search_dir, dir_entry,parfile, 'data', subdir))) :
                                  search_dirs.append(os.path.join(search_dir, dir_entry,parfile, 'data', subdir))
                                  FoundDataDir = True
                  else: 
                      print "No par file fonded"
                  simfactory = True
              else :
                  print "There is no SIMFACTORY DIR look for " , search_dir, " dir"
                  for subdir in ['Scalar', '0d', '1d']:
                      if (os.path.exists(os.path.join(search_dir, dir_entry, 'data', subdir))) :
                          search_dirs.append(os.path.join(search_dir, dir_entry, 'data', subdir))
                  if (len(search_dirs) == 0):
                      if os.path.isdir(os.path.join(search_dir, dir_entry)) == True :
                          for subdir_entry in sorted(os.listdir(os.path.join(search_dir, dir_entry))) :
                              for subdir in ['Scalar', '0d', '1d']:
                                  if (os.path.exists(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))) :
                                      search_dirs.append(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))   
                                      FoundDataDir = True
                  simfactory = True          
              if FoundDataDir == True :
                  break
          
     # ----------------############ ------------------
     # IF not-simfactory look for data dir    
     # ----------------############ ------------------
     if simfactory == False :
         #### ---------------------------------------------
         ####  Search for tree of the kind:
         ####
         ####  search_dir/[parname]/data/Scalar  
         ####                           /0d
         ####                           /1d
         #### ---------------------------------------------
         for dir_entry in sorted(os.listdir(search_dir)) :
             # There is no SIMFACTORY DIR look for "data" dir]
             for subdir in ['Scalar', '0d', '1d']:
                 if (os.path.exists(os.path.join(search_dir, dir_entry, 'data', subdir))) :
                     search_dirs.append(os.path.join(search_dir, dir_entry, 'data', subdir))
             if (len(search_dirs) == 0):
                 if os.path.isdir(os.path.join(search_dir, dir_entry)) == True :
                     for subdir_entry in sorted(os.listdir(os.path.join(search_dir, dir_entry))) :
                         for subdir in ['Scalar', '0d', '1d']:
                             if (os.path.exists(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))) :
                                 search_dirs.append(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))   
     # ----------------############ ------------------
     # Treat non-simfactory layout as flat directory    
     # ----------------############ ------------------
     if (len(search_dirs) == 0):
          search_dirs.append(search_dir)      
     for basedir in search_dirs :  
         for file_entry in sorted(os.listdir(basedir)) :
             for match in matchs :
                 if re.search(match, file_entry) :
                     matchedFILE=os.path.join(basedir,file_entry)
                     if matchedFILE not in matchedFILEs :
                         matchedFILEs.append(matchedFILE) 

     out = dict()
     for file in matchedFILEs :
         out.update(ReadASCIIFile(file,verbose=verbose,simfactory=simfactory))
     return out

#################################################################################
###
###
###
###
###
###
#################################################################################

def ReadASCIIFile_header(infile,extension,vname) :

     ##############################################
     ##
     ##   Read the ascii files to check for meaning
     ## 
     ##############################################
     if (extension == '.bz2') :
         f = bz2.BZ2File(infile)   
     elif ( extension[0] == '.gz' ) :
         f = gzip.GzipFile(infile)
     else :
         f = open(infile)
     # 
     lines = f.readlines()
     f.close()
     ##########################################

     header=[]
     header_column_found = False
     data_column_found = False
     for line in lines:
         if "#" in line:
            ###  print "Elaborating line: ",line
            if "# 1:iteration 2:time 3:data" in line:
                header = header + line.split()[1:]
                header_column_found = True
            elif "# column format:" in line:
                header = line.split()[3:]
            elif "# data columns: " in line:
                data_column_found = True
                del header[-1]
                header = header + line.split()[3:]
                break
            else :
                xx = re.match("# column[ ]*(\d*) = ([\S _]*)",line)
                if xx :
                    header = header + [str(xx.groups()[0])+":"+(xx.groups()[1]).replace(' ','_')]
                    ## print xx.groups()

     if ( header_column_found == True) & ( data_column_found == False) :
         temp1 = full_file_name.split('/')[-1] 
         temp2 = temp1.split('.')[0]
         del header[-1]
         header = header + ["3:"+temp2]
         ## print full_file_name
         ##print header

     # parse individial name fields  
     names=[]
     if ( len(header) > 0 )  :
         colnum = range(len(header)) 
         for c, name in enumerate(header):
             assert(int(name.split(":")[0]) == c+1 )
             names = names + [name.split(":")[1] ]  
             ## print name,names[c], "-->",c,colnum[c]
         # now replace entries in incols with numbers
         ## print header
     ## else :
     ## print "NOHEADER",names
     ################################################
     ## No header information found
     ###############################################
     if len(header) == 0 :
         data = np.loadtxt(infile, comments="#", unpack=True)
         Ncolumn,Nrow=np.shape(data)
         ## print "No header found, Ncolumn is ",Ncolumn
         ## print Ncolumn,Nrow
         if ( Ncolumn == 2 ) :
             ## print "This is a scalar var"
             names  = ['time','data']
             colnum = [0,1]
         elif ( Ncolumn == 3 ) :
             ## print "This is a scalar var"
             names  = ['iteration', 'time',vname]
             colnum = [0,1,2]
         elif ( Ncolumn == 13 ) :
             names  = ['iteration', 'time',vname]
             colnum = [0,8,12]
         else :
             names  = []
             colnum = []
     ## print header         
     ## print "----", colnum,names 
     return colnum,names

#################################################################################
###
###
###
###
###
###
#################################################################################
     
def ReadASCIIFile(full_file_name,verbose=False,simfactory=True) :
     out = dict()
     if not os.path.exists(full_file_name)  :
        print "File, full_file_name, do not exists:"
        return
     path , fname = os.path.split(full_file_name)
     sname, ext   = os.path.splitext(fname)
     if  ( (ext == '.bz2') or (ext == '.gz' ) ) :
         vname, ext2 = os.path.splitext(sname)
         vname = vname.replace('.','')
     else :
         vname = sname
         vname = vname.replace('.','')

     if verbose : print "Reading from file: ",full_file_name
     ## print "var Name    :",vname 
     ## print "Path: ",path
     ## print "Full Name   :",fname 
     ## print "Search Name :",sname 
     ## print "Extension :",ext 
     # --------------------------------------------------------
     # --- Now we look if full_file_name as a SIMFACTORY layout
     # --------------------------------------------------------
     SIMFACTORY=dict()
     nr = re.search("(\S*)/output-(\d\d\d\d)/(\S)",full_file_name)
     if (nr) :
         SIMFACTORY["layout"]     = True
         SIMFACTORY["basedir"]    = nr.group(1)
         SIMFACTORY["iteration"]  = int(nr.group(2))
         SIMFACTORY["files"]  = list()
         SIMFACTORY["ext"]    = list()
         for dir_entry in sorted(os.listdir(SIMFACTORY["basedir"])) :
              ## print "============================="
              #print "Scanning",dir_entry
              nrDIR = re.match(r"^output-(\d\d\d\d)$", dir_entry)
              if ( nrDIR ) :
                   ## print "GOT match =", dir_entry
                   newPATH = re.sub("output-\d\d\d\d",dir_entry,path)
                   if os.path.exists(newPATH) :
                       #print "Find path xx =", newPATH
                       for file_entry in os.listdir(newPATH) :
                            #print file_entry,"...",sname
                            #nrFILE = re.search(sname+"\S*", file_entry)
                            #if (nrFILE). 
                            if (file_entry.find(sname)<> -1):
                                ## print "Got file MATCH:", file_entry
                                SIMFACTORY["files"].append(os.path.join(newPATH, file_entry))
                                SIMFACTORY["ext"].append(os.path.splitext(file_entry)[1])
         ## print "SIMFACTORY Simulation dir:", SIMFACTORY["basedir"] 
         ## print "SIMFACTORY Run Numbers:", SIMFACTORY["iteration"]
         infile    = SIMFACTORY["files"];
         extension = SIMFACTORY["ext"];
     else :
         SIMFACTORY["layout"] = False
         infile    = [full_file_name];
         extension = [ext];


     ## -------- Get the data from the first file --------
     ##try:
     ##    data = np.loadtxt(infile[0], comments="#", unpack=True)
     ##except :
     ##    if names[0]== 'name' :
     ##        data = np.loadtxt(infile[0], comments="#", usecols=colnum[1:],unpack=True)
     ##        out['name']=np.genfromtxt(infile[0], comments="#", usecols=(0),unpack=True,dtype=np.str)
     ##        colnum = [ii-1 for ii in colnum[1:]]
     ##        names  = names[1:]
     ##        ## print names
     ##        ## print colnum
     ##        u, indices = np.unique(data[0,:], return_index=True)
     ##        for i , name in enumerate(names) :
     ##             out[name] = data[colnum[i],indices]
     ##
     ##        return out
     ##    else:
     ##        print "[ERROR 0] Unable to load file:", infile[0]
     ##        try: 
     ##            # data = np.genfromtxt(infile[0], comments="#", unpack=True)
     ##            wrongFILE = open(infile[0])
     ##            goodFILE  = open('/tmp/ReadASCIIFile.py.txt','w')
     ##            for wrongTMP in (wrongFILE.readlines()) :
     ##                if wrongTMP[0].isalnum():
     ##                    goodFILE.writelines(wrongTMP)
     ##            wrongFILE.close()
     ##            goodFILE.close()
     ##            data = np.loadtxt('/tmp/ReadASCIIFile.py.txt', comments="#",unpack=True)
     ##            os.remove('/tmp/ReadASCIIFile.py.txt')
     ##        except :
     ##            print "[ERROR 1] Unable to load file:", infile[0]
     ##            return out
     ##
     ### -----------------------------------------------------
     ### If header are not present I should try to find out
     ### the meaning of the data
     ### -----------------------------------------------------
     ##if ( len(names) == 0 )  :
     ##    Ncolumn,Nrow=np.shape(data)
     ##    ## print Ncolumn,Nrow
     ##    if ( Ncolumn == 2 ) :
     ##        ## print "This is a scalar var"
     ##        names  = ['time','data']
     ##        colnum = [0,1]
     ##    elif ( Ncolumn == 3 ) :
     ##        ## print "This is a scalar var"
     ##        names  = ['iteration', 'time',vname]
     ##        colnum = [0,1,2]
     ##    elif ( Ncolumn == 13 ) :
     ##        names  = ['iteration', 'time',vname]
     ##        colnum = [0,8,12]
     ## -------- Get the data from the first file --------

     ######################################################
     ##  Get the meaning of the column
     ######################################################
     colnum,names = ReadASCIIFile_header(infile[0],extension[0],vname)
     # -----------------------------------------------------
     # Now read data form the other components
     # -----------------------------------------------------
     for i in range(0,len(infile)) :
         ## print i
         try :
             tmp = np.loadtxt(infile[i], comments="#", unpack=True)
             if i == 0 : 
                 data = tmp
             else : 
                 data = np.append(data,tmp,axis=1)
         except :
             print "[ERROR] Unable to load file:", infile[i]

     # if there is only one row of data the array is 1d instead of 2d
     # Correct it!   
     if np.size(data.shape) == 1 :
         data = data.reshape(np.size(data),1)
     ##u, indices = np.unique(data[0,:], return_index=True)
     VALUES = data[0,::-1]
     u,  REVERSEDindices = np.unique(VALUES, return_index=True)
     indices = len(VALUES)-1 - REVERSEDindices
     for i , name in enumerate(names) :
         out[name] = data[colnum[i],indices]
 
     return out

#########################################################
##
##  Main IMPORT FUNCTIONs  for GW signal
##
#########################################################

def ImportDataPsi4RE(folder):
    psi4=dict()
    for filename in os.listdir(folder):
        REG = re.match('mp_Psi4_l(\d)_m(\d)_r(\d*).00.asc', filename)
        REG_minus = re.match('mp_Psi4_l(\d)_m-(\d)_r(\d*).00.asc', filename)
        if REG_minus:
            l = str(REG_minus.groups()[0])
            m = '-'+str(REG_minus.groups()[1])
            r = str(REG_minus.groups()[2])
        if REG:
            l = str(REG.groups()[0])
            m = str(REG.groups()[1])
            r = str(REG.groups()[2])
        if (REG or REG_minus):
            try:
                psi4[r]['l'+l+'m'+m]=dict()
            except:
                psi4[r]=dict()
                psi4[r]['l'+l+'m'+m]=dict()
            psi_tmp = ReadASCIIFile(os.path.join(folder,filename))
            ## psi_tmp = ReadASCIIFile(folder+filename)
            ## ----- print list(psi_tmp)
            psi4[r]['l'+l+'m'+m]['time']=psi_tmp['iteration']
            psi4[r]['l'+l+'m'+m]['re']=psi_tmp['time']
            psi4[r]['l'+l+'m'+m]['im']=psi_tmp[filename[:-7]+'00']
    return psi4

###########################################################
###
###       Read GRHydro_MassDiagnostic output (NEW)
###
###########################################################

def ImportMassDiagnostic(folder):
    mass = dict()
    for filename in os.listdir(folder):
        REG = re.match('grhydro_analysis-grhydro_massdiagnostic..asc',filename)
        if REG:
            mass_tmp = ReadASCIIFile(os.path.join(folder,filename))
            mass['time']     = mass_tmp['time']
            mass['Mun']      = mass_tmp['hydro_Mout']
            mass['Mrho1']    = mass_tmp['hydro_Mrho1']
            mass['Mrho2']    = mass_tmp['hydro_Mrho2']
            mass['M0']       = mass_tmp['hydro_Mb0']
            mass['Munbound'] = mass_tmp['hydro_Munbound']
            mass['M0r1']     = mass_tmp['hydro_Mb0r1']
            mass['M0r2']     = mass_tmp['hydro_Mb0r2']
    return mass

###########################################################
###                Read Outflow output (NEW)
###
###     does not work with ReadASCIIFile due to format
###     didn't work on existing function but tried to find
###                     another way to do it
###########################################################

def ImportOutflowRE(folder):
    outflow = dict()
    for filename in os.listdir(folder):
        REG = re.match('outflow_det_(\d*).asc', filename)
        if REG:
            ########### create the labels for the list
            n = str(REG.groups()[0])
            # print "Name is:  ",n,REG.groups()
            outflow[n] = dict()
            ########### now check and sort the outputs like in ReadASCIIFile
            full_file_name = folder+filename
            # print 'working on'
            # print full_file_name
            path , fname = os.path.split(full_file_name)
            sname, ext   = os.path.splitext(fname)
            ## print sname
            vname = sname
            vname = vname.replace('.','')
            nr = re.search("(\S*)/output-(\d\d\d\d)/(\S)",full_file_name)
            SIMFACTORY = dict()
            if (nr):
                SIMFACTORY["layout"]     = True
                SIMFACTORY["basedir"]    = nr.group(1)
                SIMFACTORY["iteration"]  = int(nr.group(2))
                SIMFACTORY["files"]  = list()
                SIMFACTORY["ext"]    = list()
                for dir_entry in sorted(os.listdir(SIMFACTORY["basedir"])) :
                    nrDIR = re.match(r"^output-(\d\d\d\d)$", dir_entry)
                    if ( nrDIR ) :
                        newPATH = re.sub("output-\d\d\d\d",dir_entry,path)
                        if os.path.exists(newPATH) :
                            for file_entry in os.listdir(newPATH) :
                                if (file_entry.find(sname+'.')<> -1):
                                    SIMFACTORY["files"].append(os.path.join(newPATH, file_entry))
                                    SIMFACTORY["ext"].append(os.path.splitext(file_entry)[1])
                infile    = SIMFACTORY["files"];
                extension = SIMFACTORY["ext"];
            else :
                SIMFACTORY["layout"] = False
                infile    = [full_file_name];
                extension = [ext];
            ############  read the header and makes the labels for each variables
            fo = open(os.path.join(folder,filename),"r")
            lines = fo.readlines()
            fo.close()
            names  = []
            colnum = []
            for line in lines:
                if "# 1" in line:
                    for i in range(1,10):
                        colnum.append(line.split()[i].split(':')[0])
                        names.append(line.split()[i].split(':')[1])
                        for name in names:
                            outflow[n][name] = 0
            ### print infile
            for i in range(0,len(infile)) :
                try:
                    tmp = np.loadtxt(infile[i], comments="#", unpack=True)
                    if i==0 :
                        data = tmp
                    else :
                        data = np.append(data,tmp,axis=1)
                except :
                    print "[ERROR] Unable to load file:", infile[i]
            VALUES = data[0,::-1]
            u,  REVERSEDindices = np.unique(VALUES, return_index=True)
            indices = len(VALUES)-1 - REVERSEDindices
            ## print "saving on outflow ", n 
            for i , name in enumerate(names) :
                outflow[n][name] = data[int(colnum[i])-1,indices]
    ########### return the dict containing all values
    return outflow

###########################################################
###
###########################################################

def ImportDataZerilliRE(folder) :
    ZerrilliRE = '(\S*)_Detector_Radius_(\d*).00(\S*).asc'
    listFILE = []
    DETECTOR  = set()
    DET_KEY   = set()
    data = dict()
    for filename in os.listdir(folder):
        REG = re.match(ZerrilliRE,filename)
        if REG :
            xx = REG.groups()
            if xx[2] == '' :
                pass;
                #DET_KEY.add((xx[0]).replace('Schwarzschild_',''))
            else :
                DET_KEY.add((xx[2]).replace('_',''))
            ###print filename , xx
            DETECTOR.add(xx[1])
            listFILE.append([filename,xx])
        else :
            print filename , "NO MATCH"
    for i  in DETECTOR :
        data[i] = dict()
        data[i]['time'] = [0.0]
        for j in DET_KEY:
            data[i][j] = {"even":[0],"odd":[0],'time':[0]}

    #################################
    ##
    #################################
    for filename,xx in listFILE :
        KEY2=""
        KEY3=1.0
        DET = xx[1]
        if xx[2] == '' :
            KEY=((xx[0]).replace('Schwarzschild_',''))
        else :
            KEY=((xx[2]).replace('_',''))
            if xx[0][1] == 'e':
                KEY2 = 'even' 
                if xx[0][6] == 'R':
                    KEY3 = 1.0 
                else :
                    KEY3 = 1.0j 
            else :
                KEY2 = 'odd' 
                if xx[0][5] == 'R':
                    KEY3 = 1.0 
                else :
                    KEY3 = 1.0j 
         
        #print "Reading file: ",filename,DET,KEY," (",KEY2,"-",KEY3,")"
        zer_tmp = ReadASCIIFile(os.path.join(folder,filename))
        if KEY2 == "" :
            data[DET][KEY] = zer_tmp['data']
            if len(data[DET]['time']) == 1  :
                data[DET]['time'] = zer_tmp['time']
        else :
            if len(data[DET][KEY]['time']) == 1  :
               data[DET][KEY]['time'] = zer_tmp['time']
            if len(data[DET][KEY][KEY2]) == 1  :
                data[DET][KEY][KEY2] = KEY3*zer_tmp['data']
            else:
                data[DET][KEY][KEY2] += KEY3*zer_tmp['data']

    return data

###########################################################################
###
###########################################################################



#################################################################################
###
###
###    M A I N 
###
###
#################################################################################


## -------------------------------------------------------------
##  This functions can be easly used as line tools
## -------------------------------------------------------------

if __name__ == "__main__":

    usage = "usage: "+ sys.argv[0]+" [-hv] [-p <plotvar>] [-s <shovar>] [-b BaseDir REGvars <dirs>]"

    #################################
    # PARSE command line arguments
    #################################
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hvp:P:s:b:i:o:",["ifile=","ofile="])
    except :
        print(usage)
        sys.exit(2)
    #################################
    # END PARSE command line arguments
    #################################
    if (len(args) < 1):
        print(usage)
        sys.exit(2)

    

    idx     = 0;
    BaseDir = '';
    Verbose = False
    ShowVar = False
    DoPlot  = False
    ShowPlot = False
    for opt, arg in opts:
        if ( opt == '-v') :
            Verbose = True 
        elif (opt == '-p') :
             DoPlot  = True
             PlotVar = arg 
             import matplotlib.pyplot as plt
        elif (opt == '-P') :
             DoPlot   = True
             ShowPlot = True
             PlotVar = arg 
             import matplotlib.pyplot as plt
        elif (opt == '-s') :
             ShowVar = True
             varname = arg
        elif (opt == '-b') :
             BaseDir = arg

    #############################################################
    #  Scan requested data.
    #############################################################
    x     = dict()
    xname = dict()
    idx = 0
    if BaseDir <> '' :
        ################################################################
        ##   Basedir has been set;  SCAN for regesxpression and base dir
        ###############################################################
        Regvars = args[0]
        sims    = args[1:]
        if Verbose : print "Simulations:",sims
        for sim in sorted(sims):
            try :
                idx = idx + 1
                xname[idx] = sim
                x[idx] = ReadASCIIFileRE(os.path.join(BaseDir,sim),Regvars,verbose=False)
                if Verbose : print "Data for ",os.path.join(BaseDir,sim),' filter ',Regvars
                if Verbose : print "         ",list(x[idx])
            except:
                print "Unable to process:",sim,'vars=',Regvars
    else:
        #############################################################
        ##   Basedir has not been set: scann a set of files
        #############################################################
        for file in args :
            idx = idx + 1
            xname[idx] = file
            x[idx] = ReadASCIIFile(file,verbose=False)
            if Verbose : print "Data for ",x," are ",list(x)
            
    ###############################################
    #  Prin the maximum of the requested variable
    ##############################################
    if ShowVar : 
        for i in list(x) :
            print "Maximum of (",varname,")= ",np.max(x[i][varname]),"   [",xname[i],"]"
   
    ###############################################
    #  DO A PLOT 
    ##############################################
    if DoPlot:
        plotname="temp.png"
        print "Creating plot ", plotname
        fig = plt.figure(figsize=(8, 4))
        fig.subplots_adjust(top=0.95, bottom=0.15, left=0.1,right=0.98, wspace=0.25)
        ax = fig.add_subplot(1,1,1)
        ax.plot(x[1]['time'],x[1][PlotVar])
        ax.set_title(sim)
        if ShowPlot : plt.show()
        plt.savefig(plotname, transparent=True)
        fig.clear()


    #############################################################
#            ####################################################### 
#            ## check for optional actions and fo what it is request 
#            ####################################################### 
#            for opt, arg in opts:
#                if opt == '-s' :
#                    if arg =='all' :
#                        vars=x.keys()
#                    else :
#                        vars= [arg]
#                    for var in vars :
#                        print "[",sim,"]",var,"in (", min(x[var]),max(x[var]),")"
#                if opt == '-p' :
#                    idx = idx + 1
#                    plotname='plot_'+str(idx)+"_"+arg+".pdf"
#
#            ####################################################### 
#            ####################################################### 
#        except :
#            print "Unable to process:",sim,'vars=',Regvars
