#!/usr/bin/env python

import sys
import getopt

import os
import re
import glob
import time
import numpy as np
import bz2
import gzip
import CarpetASCIII as CARPET
import CarpetH5     as H5

#################################################################################
###
###
###
###
###
###
#################################################################################


def ReadAllScalarsSimulation(name,basedir,basedirH5='',reload=False,saveH5=True) :
    h5_file_name=os.path.join(basedirH5,name +'.h5')
    if (os.path.isfile(h5_file_name) == True) and (not reload)  :
        data = H5.ReadScalarHDF5(h5_file_name)
    else:
        data = dict()
        ##data['Sc']      = CARPET.ReadASCIIFileRE(basedir+'output-0000')
        data['Sc']      = CARPET.ReadASCIIFileRE(basedir)
        data['ScMAX']   = CARPET.ReadASCIIFileRE(basedir,'maximum.asc')
        data['ScMIN']   = CARPET.ReadASCIIFileRE(basedir,'minimum.asc')
        data['ScNorm1'] = CARPET.ReadASCIIFileRE(basedir,'norm1.asc')
        data['ScNorm2'] = CARPET.ReadASCIIFileRE(basedir,'norm2.asc')
        #--------------------------
        # get WE data if present
        #--------------------------
        try :
            dirWE   = np.sort(glob.glob(basedir+'/o*-00??/*/data/WaveExtract'))[0]
            data['WE']   = CARPET.ImportDataZerilliRE(dirWE)
        except :
            print "SIM [",name,"] no dir: ",basedir+'/o*-00??/*/data/WaveExtract'
        #---------------------------------
        # get PSI4 and BH data if present
        #-------------------------------
        try : 
            dirMP = np.sort(glob.glob(basedir+'/o*-????/*/data/Multipole/'))[-1]
        except:
            dirMP = np.sort(glob.glob(basedir+'/o*-????/*/data/'))[-1]
        data['PSI4'] = CARPET.ImportDataPsi4RE(dirMP)
        data['BHH']  = CARPET.ReadASCIIFileRE(dirMP,'.gp')
        #----------------------------------
        # get OUTFLOW DATA data if present
        #----------------------------------
        data['OUTFLOW'] = CARPET.ImportOutflowRE(dirMP)
        #--------------------------------------
        # get Additional data form ".out" file
        #-------------------------------------
        try :
            fileOUT = glob.glob(basedir+'/output-????/*.out')[0]
            data['OUT'] = GetInfoOUTfile(fileOUT)
        except:
            pass;
        #--------------------------
        # WRITE H5 file
        #--------------------------
        if (os.path.isfile(h5_file_name) == True) and (reload)  and (saveH5):
            os.remove(h5_file_name)
        if (saveH5):
            H5.WriteScalarHDF5(h5_file_name,data)
    try:
        print "Sim: ", name,' t_max=',np.max(data['Sc']['time']),' ',np.max(data['Sc']['time'])/203.0
    except:
         print "Sim: ", name
    return data


#################################################################################
###
###
###
###
###
###
#################################################################################
def ReadDescriptorsSimulation(name,basedir,basedirH5='',kind='XY',reload=False) :

    #---------------------------------------------------------------------------------
    if kind == 'XY' :
        h5_file_name=os.path.join(basedirH5,'descXY_'+ name +'.h5')
    elif kind == 'XZ' :
        h5_file_name=os.path.join(basedirH5,'descXZ_'+ name +'.h5')
    elif kind == 'YZ' :
        h5_file_name=os.path.join(basedirH5,'descYZ_'+ name +'.h5')
    elif kind == 'XYZ' :
        h5_file_name=os.path.join(basedirH5,'descXYZ_'+ name +'.h5')
    else :
        return 0
    ## ------------------------------------------------------
    if (os.path.isfile(h5_file_name) == True) and (not reload) :
        desc = H5.CreateDescriptorList('',h5_file_name)
    else:
        print "Creating descriptor H5 files" 
        ## --------------- 2d ----------------
        try :
            dir2d   = np.sort(glob.glob(basedir+'/o*-000?/*/data/H5_2d'))[0]
        except :
            pass;
            print "SIM [",simname,"] no dir: ",basedir+'/o*-000?/*/data/H5_2d'
        ## --------------- 3d ----------------
        try :
            dir3d   = np.sort(glob.glob(basedir+'/o*-000?/*/data/H5_3d'))[0]
        except :
            pass;
            print "SIM [",simname,"] no dir: ",basedir+'/o*-000?/*/data/H5_3d'
        #---------------------------------------------------------------------------------
        if kind == 'XY' :
            SEARCH = os.path.join(dir2d,'*.xy.h5')
        elif kind == 'XZ' :
            SEARCH = os.path.join(dir2d,'*.xz.h5')
        elif kind == 'YZ' :
            SEARCH = os.path.join(dir2d,'*.yz.h5')
        elif kind == 'XYZ' :
            SEARCH = os.path.join(dir3d,'*.xyz.file_0.h5')
        else :
            return 0
        if (os.path.isfile(h5_file_name) == True) and (reload)  :
            os.remove(h5_file_name)
        desc = H5.CreateDescriptorList(SEARCH,h5_file_name)
    ## ------------------------------------------------------
    return desc


#################################################################################
###
###
###
###
###
###
#################################################################################


def GetInfoOUTfile(filename) :

    OUT = dict() 
    rulesINT=[
        ('nMPI', re.compile('INFO \(Carpet\): Carpet is running on (\d*) processes')),
        ('nt'  , re.compile('INFO \(Carpet\): This process contains (\d*) threads')),
        ('np'  , re.compile('INFO \(Carpet\): There are (\d*) threads in total')),
        ('nTx' , re.compile('INFO \(Carpet\): There are (\d*) threads per process')),
        ('memMB',re.compile('\s*Current allocated memory:  (\d*).(\d*) MB'))
    ]
    rulesFLOAT=[
        ('TOTmemGB',re.compile("INFO \(Carpet\): Total required memory: (\S*) GByte \(for GAs and currently active GFs\)") ),
        ('dx[0]' , re.compile("INFO \(CartGrid3D\): dx=>([\S.+]*)  dy=>([\S.+]*)  dz=>([\S.+]*)") ),
        ('omega' , re.compile('Orbital angular velocity : (\S*) rad/s')   ),
        ('M0'    , re.compile('Baryon mass :\s*(\S*) M_sol')              ),
        ('M'     , re.compile('Gravitational mass : (\S*) M_sol')         ),
        ('Madm'  , re.compile('\s*ADM mass of the system : (\S*) M_sol')  ),
        ('Jadm'  , re.compile('\s*Total angular momentum : (\S*)')        ),
        ('BNS_omega'  , re.compile('INFO \(Meudon_Bin_NS\): omega \[rad/s\]:       ([\S.+-]*)' )  ),
        ('BNS_dist'  , re.compile('INFO \(Meudon_Bin_NS\): dist \[km\]:           ([\S.+-]*)' )  ),
        ('BNS_dist_mass'  , re.compile('INFO \(Meudon_Bin_NS\): dist_mass \[km\]:      ([\S.+-]*)' )  ),
        ('BNS_mass1_b'  , re.compile('INFO \(Meudon_Bin_NS\): mass1_b \[M_sun\]:     ([\S.+-]*)' )  ),
        ('BNS_mass2_b'  , re.compile('INFO \(Meudon_Bin_NS\): mass2_b \[M_sun\]:     ([\S.+-]*)' )  ),
        ('BNS_mass_ADM'  , re.compile('INFO \(Meudon_Bin_NS\): mass_ADM \[M_sun\]:    ([\S.+-]*)' )  ),
        ('BNS_L_tot'  , re.compile('INFO \(Meudon_Bin_NS\): L_tot \[G M_sun^2/c\]: ([\S.+-]*)' )  ),
        ('BNS_rad1_x_comp'  , re.compile('INFO \(Meudon_Bin_NS\): rad1_x_comp \[km\]:    ([\S.+-]*)' )  ),
        ('BNS_rad1_y'  , re.compile('INFO \(Meudon_Bin_NS\): rad1_y \[km\]:         ([\S.+-]*)' )  ),
        ('BNS_rad1_z'  , re.compile('INFO \(Meudon_Bin_NS\): rad1_z \[km\]:         ([\S.+-]*)' )  ),
        ('BNS_rad1_x'  , re.compile('INFO \(Meudon_Bin_NS\): rad1_x_opp \[km\]:     ([\S.+-]*)' )  ),
        ('BNS_rad2_x'  , re.compile('INFO \(Meudon_Bin_NS\): rad2_x_comp \[km\]:    ([\S.+-]*)' )  ),
        ('BNS_rad2_y'  , re.compile('INFO \(Meudon_Bin_NS\): rad2_y \[km\]:         ([\S.+-]*)' )  ),
        ('BNS_rad2_z'  , re.compile('INFO \(Meudon_Bin_NS\): rad2_z \[km\]:         ([\S.+-]*)' )  ),
        ('BNS_rad2_x_opp'  , re.compile('INFO \(Meudon_Bin_NS\): rad2_x_opp \[km\]:     (\S*)' )  )
    ]
    rulesFLOAT3=[
        ('dx' , re.compile("INFO \(CartGrid3D\): dx=>([\S.+]*)  dy=>([\S.+]*)  dz=>([\S.+]*)") )
    ]

    f = open(filename) 
    lines=f.readlines()
    f.close()
    for rule in rulesINT:
        ## print rule
        xx=filter(rule[1].match,lines)
        if len(xx) > 0 :
            x1=rule[1].match(xx[0])
            if x1:
                OUT[rule[0]] = np.int(x1.groups()[0])
    for rule in rulesFLOAT:
        ## print rule
        xx=filter(rule[1].match,lines)
        if len(xx) > 0 :
            x1=rule[1].match(xx[0])
            if x1:
                OUT[rule[0]] = np.float(x1.groups()[0])
    for rule in rulesFLOAT3:
        ## print rule
        xx=filter(rule[1].match,lines)
        if len(xx) > 0 :
            x1=rule[1].match(xx[0])
            if x1:
                OUT[rule[0]] = np.array([np.float(x1.groups()[0]),np.float(x1.groups()[1]),np.float(x1.groups()[2])])
    return OUT
