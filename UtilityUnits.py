#!/usr/bin/env python 

# ----------------------------------------------------------------------
#   Constant set from the value present in LORENE
#   Lorene/C++/Include/unites.h
#
#   const double g_si = 6.6738E-11 ;     ///< Newton gravitational constant [SI]
#   const double c_si = 2.99792458E+8 ;  ///< Velocity of light [m/s]
#   const double kB_si = 1.3806488E-23 ; ///< Boltzmann constant [J/K]
#   const double rhonuc_si = 1.66E+17 ;  ///< Nuclear density [kg/m3] (arbitrary)
#   const double km_si = 1.E+3 ;         ///< One kilometer [m]
#   const double msol_si = 1.9885E+30 ;  ///< Solar mass [kg]
#   const double mev_si = 1.602176565E-13 ;   ///< One MeV [J]
#
#   const double mu_si = 1.2566370614359173e-6 ;///<Magnetic vacuum permeability
#
# constants, in SI
#
# http://www-pdg.lbl.gov/2016/reviews/rpp2016-rev-phys-constants.pdf
# http://www-pdg.lbl.gov/2016/reviews/rpp2016-rev-astrophysical-constants.pdf
#   G     = 6.67408e-11     # m^3/(kg s^2)
#   c     = 299792458.0     # m/s
#   cCGS  = 299792458.0*100 # cm/s
#   M_sun = 1.98848e30      # kg
#   mu0   = 1.2566370614e-6 # Newton/Ampere^2
#            4*\pi*1e-7
#   Kb      = 1.38064852e-23    # Joule/K
#   Mparsec = 3.08567758e19     # km
#   eV      = 1.6021766208e-19  # Joule
# ------------------------------------------------------------------------
G     = 6.6738e-11     # m^3/(kg s^2)
c     = 299792458.0     # m/s
cCGS  = 299792458.0*100 # cm/s
M_sun = 1.9885e30       # kg
mu0   = 1.256637061435917e-6 # Newton/Ampere^2
                        # 4*\pi*1e-7
Kb      = 1.3806488e-23     # Joule/K
Mparsec = 3.08567758e19     # km
eV      = 1.6021766208e-19  # Joule
MeV     = 1.6021766208e-13  # Joule
MeVCGS  = 1.6021766208e-6   # g cm^2/s^2
fermi   = 1e-15             # metri
fermiCGS= 1e-13             # metri
##----------------------------------
## The Barion Mass used by RNS
##----------------------------------
Mbaryon    = 1.66e-27     # kg
#-------------------------------------------
# conversion factors
#-------------------------------------------
CU_to_kg   = M_sun                               # kg
CU_to_s    = (M_sun*G/(c*c*c))                   # s
CU_to_m    = M_sun*G/(c*c)                       # m
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_press= c**6 / (G**3 * M_sun**2) *c**2      # kg m/s^2 /m^2
CU_to_Tesla =c**4 / M_sun / G**(1.5)* mu0**(0.5)
CU_to_Gauss =c**4 / M_sun / G**(1.5)* mu0**(0.5) * 10000
CU_to_energy    = M_sun*c*c                # kg m^2/s^2
######## Usefull #############################################
CU_to_km   = CU_to_m/1000                   # km
CU_to_ms   = 1000*CU_to_s                   # ms
########### CGS ############################################## 
MbaryonCGS      = Mbaryon*1000              # g
CU_to_cm        = CU_to_km * 1000 * 100     # cm
CU_to_g         = CU_to_kg * 1000           # g
CU_to_energyCGS = CU_to_energy *1000*100**2 # g cm^2/s^2
CU_to_densCGS   = CU_to_dens   *1000/100**3 # g/cm^3
CU_to_pressCGS  = CU_to_press  *1000/100    # g /s^2 / cm
# ------------------------------------------------------------
# All expeession in CU refer to unit mass (a-dimensional) 
# where G=c=M_sun=1
# ------------------------------------------------------------

#--------------------------------------------------------------------
# ORIGINAL RNS values
#
# #define C 2.9979e10                  /* speed of light in vacuum */
# #define G 6.6732e-8                  /* gravitational constant */ 
# #define MSUN 1.987e33                /* Mass of Sun */
# #define MB 1.66e-24                  /* baryon mass */
#
# #define KAPPA (1.0e-15*C*C/G)        /* scaling factor */
# #define KSCALE (KAPPA*G/(C*C*C*C))   /* another scaling factor */
# #define SQ(x) ((x)*(x))              /* square macro */
# #define RMIN 1.0e-15                 /* use approximate TOV equations when
# #computing spherical star and r<RMIN */
#--------------------------------------------------------------------

RNS_c     = 2.9979e10
RNS_G     = 6.6732e-8
RNS_MB    = 1.66e-24
RNS_M_sun = 1.987e33
CU_RNS_to_dens  = RNS_c**6 / (RNS_G**3 * RNS_M_sun**2)
CU_RNS_to_press = RNS_c**6 / (RNS_G**3 * RNS_M_sun**2) *RNS_c**2
